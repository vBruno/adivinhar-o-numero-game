import random

numDesconhecido = random.randint(1, 15)

numDigitado = int(input("Digite um número: "))

while numDesconhecido != numDigitado:
    if numDigitado > numDesconhecido:
        print("Número digitado alto!")
    else:
        print("Número digitado baixo!")
    numDigitado = int(input("Digite um número: "))
else:
    print(f"Parabéns, você acertou o número: {numDesconhecido}")


