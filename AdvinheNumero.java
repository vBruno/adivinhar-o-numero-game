import java.util.Scanner;
import java.util.Random;

public class AdvinheNumero{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        Random aleatorio = new Random();

        int numDesconhecido = aleatorio.nextInt(15) + 1;
        System.out.println(numDesconhecido);

        System.out.println("Digite um número: ");
        int numDigitado = leia.nextInt();

        while(numDesconhecido != numDigitado){
            if(numDigitado > numDesconhecido){
                System.out.println("Número alto!");
            }else{
                System.out.println("Número baixo!");
            }
            System.out.println("Digite um número: ");
            numDigitado = leia.nextInt();
        }
        System.out.println("Parabéns, você acertou o número");
    }
}